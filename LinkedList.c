/******************************************************************************
 * LINKED LIST                                                                *
 *                                                                            *
 * THIS PROGRAM CREATES A LINKED LIST AND CAN INSERT, APPEND, REMOVE, SORT,   *
 * AND SEARCH NODES IN ADDITION TO CHECKING THE NODES FOR REDUNDANCY.         *
 *                                                                            *
 * AUTHOR: LAWRENCE MAKIN                                                     *
 * DATE: 19/03/2021                                                           *
 ******************************************************************************/

/* Pre-Processor Directives */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Function Prototypes */
typedef struct node
{
	char *data;			/* Can refactor with any sortable data e.g. int */
	struct node *next;
} NODE;

/* Subroutines */
NODE *createNode(char *text)
{
	NODE *newNode = (NODE *) calloc(1, sizeof(NODE));
	newNode->data = (char *) calloc(strlen(text) + 1, sizeof(char));
	strcpy(newNode->data, text);
	newNode->next = NULL;
}

int main(int argc, char **argv)
{
	NODE *new = createNode("Test\n");
	printf("%s", new->data);
	free(new);
}